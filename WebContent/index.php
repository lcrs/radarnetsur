<?php
session_start();
$navigation = htmlspecialchars($_GET["n"]);
$lang = htmlspecialchars($_GET["lang"]);
if($lang == ""){
	$lang = "en";
}
$radar_id = htmlspecialchars($_GET["radar_id"]);
if($radar_id == ""){
	$radar_id = "SE";
}
$radar_folder = htmlspecialchars($_GET["radar_folder"]);
$radarData = array(array( array("LOXX", "Loja (LOXX, 60km)"),
               array("LOX1", "Loja (30km)"),
               array("LOX3", "Loja (15km)")
             ),
		array( array("CAXX", "Cajas (CAXX, 100km)"),
               array("CAX1", "Cajas (60km)"),
               array("CAX3", "Cajas (20km)")
             ),
		array( array("GUAXX", "Celica (GUAXX, 100km)"),
               array("GUAX1", "Celica (60km)"),
               array("GUAX3", "Celica (20km)")
             ));
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Radar Network South Ecuador Test site </title>
        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="js/jquery.tmf_tooltip.js"></script>
        <script type="text/javascript" src="js/jquery.fancybox.pack.js"></script>
        <script type="text/javascript">
        	var serverTime = <?php echo time(); ?>;
        	$(document).ready(function() {
	        	$(".toolTip").each(function(index) {
	        		$(this).simpleTooltip();
	        	});
	        	$(".popup_fancybox").fancybox();
        	});
        </script>
        <?php
        	if($radar_id == "SE"){
        		echo '<script src="js/radar_SE.js" type="text/javascript"></script>';
        	}else{
        		echo '<script src="js/radar_v3.js" type="text/javascript"></script>';
			}
        ?>
        <link rel="stylesheet" type="text/css" href="css/style.css" />
    </head>
    <body>
        <div class="wrapper">
            <div id="header">
                <a class="home" href="index.php">Radar Network South Ecuador</a>
                <a class="headerLogo" href="http://www.prefecturaloja.gob.ec" target="_blank">
                	<img src="images/Identidad_Prefectura01_small.png" alt="www.prefecturaloja.gob.ec" />
                </a>
                <a class="headerLogo" href="http://www.dfg.de" target="_blank">
                	<img src="images/dfg_logo_short_en.png" alt="www.dfg.de" />
                </a>
                <div class="clear">
                    <!-- clear -->
                </div>
            </div>
            <div id="colmask">
				<div id="colmid">
					<div id="colright">
						<div id="col1wrap">
							<div id="col1pad">
								<div id="col1">
									<!-- content -->
									<?php
				                		if($navigation == "instrument_info"){
											include 'inc/instrument_info.php';
										}else if($navigation == "instrument_photo"){
											include 'inc/instrument_photo.php';
										}else if($navigation == "principles"){
											include 'inc/functional_principles.php';
										}else {
											include 'inc/radar.php';
										}
				                	?>
								</div>
							</div>
						</div>
						<div id="col2">
							<!-- left sidebar -->
							<?php
		                    	include 'inc/sidebar.php';
							?>
						</div>
						<div id="col3">
							<!-- right sidebar -->
						</div>
					</div>
				</div>
			</div>
            <div id="footer">
                <a href="http://www.lcrs.de" target="_blank"> <img src="images/lcrs.png" alt="www.lcrs.de" title="www.lcrs.de"/> </a>
                <a href="http://www.utpl.edu.ec" target="_blank"> <img src="images/utpl.png" alt="www.utpl.edu.ec" title="www.utpl.edu.ec"/> </a>
                <a href="http://www.etapa.net.ec" target="_blank"> <img src="images/etapa.png" alt="http://www.etapa.net.ec" title="http://www.etapa.net.ec"/> </a>
                <a href="http://www.naturalezaycultura.org" target="_blank"> <img src="images/nci.png" alt="www.naturalezaycultura.org" title="www.naturalezaycultura.org"/> </a>
                <a href="http://www.tropicalmountainforest.org" target="_blank"> <img src="images/mrpse.png" alt="www.tropicalmountainforest.org" title="www.tropicalmountainforest.org"/> </a>
                <span class="footerText">Supported by:&nbsp;</span>
                <div class="clear"><!-- clear --></div>
            </div>
        </div>
    </body>
</html>
