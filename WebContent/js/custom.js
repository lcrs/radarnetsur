var t;
var txt = "UTC";
var index = 0;
var speed = 1000;
var direction = "forward";

function parseFilename(file){
    year = "20" + file.substr(2, 2);
    month = file.substr(4, 2);
    day = file.substr(6, 2);
    hour = file.substr(8, 2);
    minute = file.substr(10, 2);
    timestring = day + "." + month + "." + year + ", " + hour + ":" + minute;
    return timestring;
}

function initiateTimer(){
    t = window.setInterval('emulateClick()', speed);
}

function emulateClick(){
    $("#button_next").click();
}

function deleteTimer(){
    clearInterval(t);
}

function checkIndex(i){
    var tmp;
    if (i > picArray.length) {
        tmp = i - picArray.length;
    }
    else 
        if (i < 0) {
            tmp = picArray.length + i;
        }
        else {
            tmp = i;
        }
    return tmp;
}

function renderImages(i){
    a = checkIndex(i - 2);
    b = checkIndex(i - 1);
    c = checkIndex(i);
    d = checkIndex(i + 1);
    e = checkIndex(i + 2);
    $("#picContainer").append(addImgElement(a)).append(addImgElement(b)).append(addImgElement(c)).append(addImgElement(d)).append(addImgElement(e));
}

function addImgElement(i){
    return '<img src="' + picArray[i] +
    '" alt="' +
    parseFilename(picArray[i]) +
    " " +
    txt +
    '"/>';
}

function initiate(){
	index = 0;
    $("#picContainer img").remove();
	renderImages(index);
    $("#picTitle").text($("#picContainer img:nth-child(3)").attr("alt"));
    $("#picContainer img:nth-child(3)").show();
    $("#dateOverlay").text($("#picContainer img:nth-child(3)").attr("alt")).show();
    $("#picIndex").text(index + "/" + picArray.length);
}

$(document).ready(function(){
    $("#button_previous").click(function(){
        index--;
        index = checkIndex(index);
        if (direction == "backward") {
            $("#picContainer").prepend(addImgElement(checkIndex(index-2)));
            $("#picContainer img:last-child").remove();
            $("#picTitle").text($("#picContainer img:nth-child(3)").attr("alt"));
            $("#dateOverlay").text($("#picContainer img:nth-child(3)").attr("alt"));
            $("#picIndex").text(index + "/" + picArray.length);
        }
        else {
            $("#picContainer img").remove();
            renderImages(index);
            direction = "backward";
        }
        $("#picContainer img:nth-child(3)").show();
        $("#picContainer img:nth-child(4)").hide();
        $("#picContainer img:nth-child(5)").hide();
    });
    $("#button_next").click(function(){
        index++;
        index = checkIndex(index);
        if (direction == "forward") {
            $("#picContainer").append(addImgElement(checkIndex(index+2)));
            $("#picContainer img:first-child").remove();
            $("#picTitle").text($("#picContainer img:nth-child(3)").attr("alt"));
            $("#dateOverlay").text($("#picContainer img:nth-child(3)").attr("alt"));
            $("#picIndex").text(index + "/" + picArray.length);
        }
        else {
            $("#picContainer img").remove();
            renderImages(index);
            direction = "forward";
        }
        $("#picContainer img:nth-child(1)").hide();
        $("#picContainer img:nth-child(2)").hide();
        $("#picContainer img:nth-child(3)").show();
    });
    $("#button_play").click(function(){
        initiateTimer();
        $(this).hide();
        $("#button_stop").show();
        $("#button_slower").show();
        $("#button_faster").show();
        $("#button_previous").hide();
        $("#button_next").hide();
    });
    $("#button_stop").click(function(){
        deleteTimer();
        $(this).hide();
        $("#button_play").show();
        $("#button_slower").hide();
        $("#button_faster").hide();
        $("#button_previous").show();
        $("#button_next").show();
    });
    $("#button_slower").click(function(){
        deleteTimer();
        speed = speed * 2;
        initiateTimer();
    });
    $("#button_faster").click(function(){
        deleteTimer();
        speed = speed / 2;
        initiateTimer();
    });
    initiate();
    //+----------+
    //| overlays |
    //+----------+
    $("#map").click(function(){
        $("#picMap").toggle();
    });
    $("#map").attr('checked', true);
    $("#roads").click(function(){
        $("#roadsOverlay").toggle();
    });
    $("#roads").attr('checked', false);
    $("#rivers").click(function(){
        $("#riversOverlay").toggle();
    });
    $("#rivers").attr('checked', false);
    $("#towns").click(function(){
        $("#townsOverlay").toggle();
    });
    $("#towns").attr('checked', true);
    $("#picContainer img:nth-child(3)").show();
});
