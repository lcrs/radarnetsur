var objectArray = new Array();
var txt = "";
var speed = 1000;
var index = 0;
var t;
var picArray;
function checkIndex(array, i){
    var tmp;
    var test = "";
    if (i >= array.length) {
        tmp = i - array.length;
    }
    else {
        if (i < 0) {
            tmp = array.length + i;
        }
        else {
            tmp = i;
        }
    }
    return tmp;
}

function initiateTimer(){
    t = window.setInterval('emulateClick()', speed);
}

function emulateClick(){
    $("#button_next").click();
}

function deleteTimer(){
    clearInterval(t);
    t = null;
}

function previous(target){
    $(target).prepend(addImgElement(checkIndex(picArray, index - 2)));
    $(target).find("img:last-child").remove();
    $(target).find("img:nth-child(3)").show();
    $(target).find("img:nth-child(4)").hide();
    $(target).find("img:nth-child(5)").hide();
}

function next(target){
    $(target).append(addImgElement(checkIndex(picArray, index + 2)));
    $(target).find("img:first-child").remove();
    $(target).find("img:nth-child(1)").hide();
    $(target).find("img:nth-child(2)").hide();
    $(target).find("img:nth-child(3)").show();
}

function addImgElement(i){
    var tmpMeasures;
    if (radar_id != "SE") {
        tmpMeasures = ' width="' + $("#picContainer").width() + '" height="' + $("#picContainer").height() + '"';
    }
    else {
        tmpMeasures = ' width="' + $(".specialRadarOverlay").width() + '" height="' + $(".specialRadarOverlay").height() + '"';
    }
    return '<img src="areas/' + radar_id + "/radarimages/" + radar_folder + "/" + picArray[i] +
    '?t=' +
    serverTime +
    '" alt="' +
    parseFilename(picArray[i]) +
    " " +
    txt +
    '"' +
    tmpMeasures +
    '/>';
}

function parseFilename(file){
	var tmp = file.indexOf(".");
	year = file.substr(tmp-12, 4);
	month = file.substr(tmp-8, 2);
	day = file.substr(tmp -6, 2);
	hour = file.substr(tmp -4, 2);
	minute = file.substr(tmp -2, 2);
	timestring = day + "." + month + "." + year + ", " + hour + ":" + minute;
	return timestring;
}

$(document).ready(function() {
	$('#additionalControls [type="checkbox"]').click(function() {
		$("#" + $(this).attr("id") + "Overlay").toggle();
	}).attr('checked', true);
	$("#button_previous").click(function(){
	    index--;
	    index = checkIndex(picArray, index);
	    $("#picTitle").text($(radar_target).find("img:nth-child(3)").attr("alt") + " / Speed: " + 1000 * speed);
        $("#dateOverlay").text($(radar_target).find("img:nth-child(3)").attr("alt"));
        $("#picIndex").text((index + 1) + "/" + picArray.length);
	    previous(radar_target);
	});
	$("#button_next").click(function(){
	    index++;
	    index = checkIndex(picArray, index);
	    $("#picTitle").text($(radar_target).find("img:nth-child(3)").attr("alt") + " / Speed: " + 1000 / speed);
        $("#dateOverlay").text($(radar_target).find("img:nth-child(3)").attr("alt"));
        $("#picIndex").text((index + 1) + "/" + picArray.length);
	    next(radar_target);
	});
	$("#button_play").click(function(){
	    if (!t) {
	        initiateTimer();
	    }
	    $(this).hide();
	    $("#button_stop").show();
	    $("#button_slower").show();
	    $("#button_faster").show();
	    $("#button_previous").hide();
	    $("#button_next").hide();
	});
	$("#button_stop").click(function(){
	    deleteTimer();
	    $(this).hide();
	    $("#button_play").show();
	    $("#button_slower").hide();
	    $("#button_faster").hide();
	    $("#button_previous").show();
	    $("#button_next").show();
	});
	$("#button_slower").click(function(){
	    if (speed < 5000) {
	        deleteTimer();
	        speed = speed * 2;
	        initiateTimer();
	    }
	});
	$("#button_faster").click(function(){
	    if (speed > 32) {
	        deleteTimer();
	        speed = speed / 2;
	        initiateTimer();
	    }
	});
	if(picArray){
		$("#dateOverlay").text($(radar_target).find("img:nth-child(3)").attr("alt")).show();
		$("#picIndex").text((index + 1) + "/" + picArray.length).show();
		$("#button_play").click();
	}
});