<h4>Meteorological Radar - How does it work?</h4>
<p>
	The Radar emits strong pulses of electro-magnetic waves. These waves are reflected by rain drops. The reflection is then captured back by the same antenna and the strength of the reflection is proportional to the intensity of the rain.
	<br/>
	From the position of the antenna and the return time of the signal, the position of the rain field can be determined. This is converted in the processing computer, to give a visual map of the rain distribution.
	<br/>
	By this the radar can give the information which would require several thousands of normal rain gauges read out each five minutes.
</p>
<p>
	Problems:
	<br/>
	<b>Clutter fields</b>
	<br/>
	When the radar beam hits the ground or mountains, strong reflections are produced. These reflections are static, they don´t change with time and so can be filtered by the processing computer. Such areas are then interpolated, to give a best estimate of the rain fall there.
</p>
<p>
	<b>Beam shadow</b>
	<br/>
	The radar beam is blocked by high mountains, valleys behind that mountains are not observed by the radar. But this is only a problem, when the clouds are very low.
</p>
<p>
	<b>Weakening of the radar beam</b>
	<br/>
	When the radar beam has crossed a rain shower, its energy is weaker. Hence, a second rain shower behind the first one may be shown weaker, than it actually is.
</p>