<a href="#" style="text-decoration:none" id="en" class="language">
	<img src="images/gb.png" alt="english"/>
</a>
<a href="#" style="text-decoration:none" id="es" class="language">
	<img src="images/es.png" alt="spanish"/>
</a>
<script type="text/javascript">
    function removeParam(key, sourceURL) {
        var rtn = sourceURL.split("?")[0],
            param,
            params_arr = [],
            queryString = (sourceURL.indexOf("?") !== -1) ? sourceURL.split("?")[1] : "";
        if (queryString !== "") {
            params_arr = queryString.split("&");
            for (var i = params_arr.length - 1; i >= 0; i -= 1) {
                param = params_arr[i].split("=")[0];
                if (param === key) {
                    params_arr.splice(i, 1);
                }
            }
            rtn = rtn + "?" + params_arr.join("&");
        }
        return rtn;
    }
    $(".language").click(function(event){
		event.preventDefault();
		alteredUrl = removeParam("lang",window.location.href);
		if(alteredUrl.indexOf("?")<0){
			urlTmp = "?lang=";
		}else{
			urlTmp = "&lang=";
		}		
		window.location.replace(alteredUrl+urlTmp+$(this).attr("id"));
	});
</script>