					<?php 
						$radar_pics = array();
						// functions
						function parseFilename($file){
							$tmp = strrpos($file,".");
							$year = substr($file, $tmp-12, 4);
							$month = substr($file, $tmp-8, 2);
							$day = substr($file, $tmp-6, 2);
							$hour = substr($file, $tmp-4, 2);
							$minute = substr($file, $tmp-2,2);
							$timestring = $day . "." . $month . "." . $year . ", " . $hour . ":" . $minute;
							return $timestring;
						}
						
						function checkFileExtension($f){
							if(!strrpos($f, "jpg") && !strrpos($f, "png") && !strrpos($f, "gif") && !strrpos($f, "jpeg") && !strrpos($f, "bmp")){
								return false;
							}
							return true;
						}
						
						function checkFile($id, $overlay){
							if($id != ""){
								$url = 'areas/'.$id.'/overlays/'.$overlay.'.png';
								if (file_exists($url)) {
								    echo '<img alt="" src="'.$url.'"/>';
								} else {
								    echo '<img alt="no pic" src="images/exclamation.png"/>';
								}
							}else{
								echo $target;
							}
						}
						// functions end
						if ($handle = opendir('areas/'.$radar_id.'/radarimages/'.$radar_folder)) {
						    while (false !== ($file = readdir($handle))) {
						        if ($file != "." && $file != ".." && checkFileExtension($file)) {
						        	array_push($radar_pics,sprintf("%s", $file));
						        }
						    }
						    closedir($handle);
						}
						sort($radar_pics);
						echo '<script type="text/javascript">';
						echo '	var radar_id = "'.$radar_id.'";'."\n";
						echo '	var radar_folder = "'.$radar_folder.'";'."\n";
						if($radar_id != "SE"){
							echo '	var radar_target = "#picContainer";'."\n";
						}
						if(count($radar_pics) > 0){
							$tmp = "";
							for ($i = 0; $i < count($radar_pics); $i++) {
								$tmp .= '"'.$radar_pics[$i].'", ';
							}
							$tmp = substr($tmp,0,-2);
							echo '	var picArray = new Array('.$tmp.');'."\n";
						}
						echo '	function setZoom(id,folder,zoomOut){'."\n";
						echo '		$("#mapFrame").click(function(){'."\n";
						echo '			window.location = "index.php?radar_id="+id+"&radar_folder="+folder;'."\n";
						echo '		}).attr("title","click to zoom in");'."\n";
						echo '		$(".zoomIn").attr("href","index.php?radar_id="+id+"&radar_folder="+folder);'."\n";
						echo '		$(".zoomOut").attr("href","index.php?radar_id="+zoomOut+"&radar_folder="+folder);'."\n";
						echo '	};'."\n";
						echo '	$(document).ready(function() {;'."\n";
						for ($i = 0; $i < count($radarData); $i++) {
							for ($j = 0; $j < count($radarData[$i]); $j++) {
								if($radarData[$i][$j][0] == $radar_id){
									echo '		setZoom("'.$radarData[$i][$j+1][0].'","'.$radar_folder.'","'.$radarData[$i][$j-1][0].'");'."\n";
									echo '		$("#mapFrame").addClass("zoom");'."\n";
									echo '		$("#mapFrame").mouseover(function() {'."\n";
									echo '			$("#specialOverlayPic").css("background","url(areas/'.$radar_id.'/overlays/nextzoom.png)");'."\n";
									echo '		});'."\n";
									echo '		$("#mapFrame").mouseout(function() {'."\n";
									echo '			$("#specialOverlayPic").css("background","none");'."\n";
									echo '		});'."\n";
									echo '		$("#col1 h2").text("'.$radarData[$i][$j][1].'");';
								}
							}
						}
						if($radar_id != "SE"){
							echo '	$("#zoomControls").show();'."\n";
						}
						echo '	});'."\n";
						echo '</script>';
					?>
					<noscript>
                		<div class="errorfield">
                			Please activate JavaScript in your browser for this site to work properly.
                		</div>
                	</noscript>
                    <h2> Southern Ecuador </h2>
                    <div id="radarElementContainer">
	                    <div id="radarElements">
	                        <div id="backgroundOverlay">
	                            <!-- background -->
	                            <?php
	                            	checkFile($radar_id, "background");
	                            ?>
	                        </div>
	                        <div id="terrainOverlay">
	                            <!-- terrain -->
	                            <?php
	                            	checkFile($radar_id, "terrain");
	                            ?>
	                        </div>
	                        <div id="roadsOverlay">
	                            <!-- roads -->
	                            <?php
	                            	checkFile($radar_id, "roads");
	                            ?>
	                        </div>
	                        <div id="riversOverlay">
	                            <!-- rivers -->
	                            <?php
	                            	checkFile($radar_id, "rivers");
	                            ?>
	                        </div>
	                        <div id="townsOverlay">
	                            <!-- towns -->
	                            <?php
	                            	checkFile($radar_id, "towns");
	                            ?>
	                        </div>
	                        <div id="specialOverlay">
	                            <!-- special -->
	                            <div id="specialOverlayPic">
	                            	<!-- pic -->
	                            	<?php
		                            	checkFile($radar_id, "special");
		                            ?>
	                            </div>
	                        </div>
	                        <div id="shadowOverlay">
	                            <!-- shadow -->
	                            <div id="shadowOverlayPic">
	                            	<!-- pic -->
	                            	<?php
		                            	checkFile($radar_id, "shadow");
		                            ?>
	                            </div>
	                        </div>
	                        <div id="picContainer">
	                            <!-- images -->
	                            <?php 
								if($radar_id != "SE"){
									for ($i = 0; $i < 5; $i++) {
										echo '<img alt="'.parseFilename($radar_pics[$i]).'" src="areas/'.$radar_id.'/radarimages/'.$radar_folder.'/'.$radar_pics[$i].'"/>';
									}
								}
	                            ?>
	                        </div>
	                        <div id="scale">
	                            <div id="scaleTextTop" class="scaleText">
	                                high
	                            </div>
	                            <div id="scaleTextBottom" class="scaleText">
	                                low
	                            </div>
	                        </div>
	                        <div class="clear">
	                            <!-- clear -->
	                        </div>
	                        <div id="controls" style="width:100px">
	                            <span class="button_control" id="button_previous" title="Previous picture"> <!-- previous --> </span>
	                            <span class="button_control" id="button_slower" title="Slower animation"> <!-- slower --> </span>
	                            <span class="button_control" id="button_play" title="Play"> <!-- play --> </span>
	                            <span class="button_control" id="button_stop" title="Pause"> <!-- stop --> </span>
	                            <span class="button_control" id="button_faster" title="Faster animation"> <!-- faster --> </span>
	                            <span class="button_control" id="button_next" title="Next picture"> <!-- next --> </span>
	                            <a href="#help" class="button_control toolTip popup_fancybox" id="button_help" title="How to use the controls"> <!-- help --> </a>
	                            <div class="clear">
	                                <!-- clear -->
	                            </div>
	                        </div>
	                        <div id="mapFrame">
	                        	<?php
	                        		if($radar_id == "SE"){
	                        			$directories = glob('areas/CAXX/radarimages' . '/*' , GLOB_ONLYDIR);
			                        	echo '<a id="specialRadarOverlay1" href="index.php?radar_id=CAXX&amp;radar_folder='.substr($directories[0],strrpos($directories[0],'/')+1).'" class="specialRadarOverlay">';
				                        echo '    <!-- special -->';
				                        echo '</a>';
										$directories1 = glob('areas/GUAXX/radarimages' . '/*' , GLOB_ONLYDIR);
				                        echo '<a id="specialRadarOverlay2" href="index.php?radar_id=GUAXX&amp;radar_folder='.substr($directories1[0],strrpos($directories1[0],'/')+1).'" class="specialRadarOverlay">';
				                        echo '    <!-- special -->';
				                        echo '</a>';
										$directories2 = glob('areas/LOXX/radarimages' . '/*' , GLOB_ONLYDIR);
				                        echo '<a id="specialRadarOverlay3" href="index.php?radar_id=LOXX&amp;radar_folder='.substr($directories2[0],strrpos($directories2[0],'/')+1).'" class="specialRadarOverlay">';
				                        echo '    <!-- special -->';
				                        echo '</a>';
									}
									?>
	                            <div id="dateOverlay">
		                            <span><!--date --></span>
		                        </div>
		                        <div id="picIndex">
		                            <!--index -->
		                        </div>
		                        <div id="zoomControls">
		                            <!--zoom -->
		                            <a href="#" class="zoomIn" title="zoom in">
		                            	<!--zoom in -->
		                            </a>
		                            <a href="#" class="zoomOut" title="zoom out">
		                            	<!--zoom out -->
		                            </a>
		                        </div>
	                        </div>
	                        <div id="additionalControls">
	                        	<?php
	                        		if (file_exists('areas/'.$radar_id.'/overlays/terrain.png')) {
									    echo '<input id="terrain" type="checkbox" checked="checked"/>';
	                            		echo '<label for="terrain"> Terrain </label>';
									}
									if (file_exists('areas/'.$radar_id.'/overlays/roads.png')) {
									    echo '<input id="roads" type="checkbox" checked="checked"/>';
	                            		echo '<label for="roads"> Roads </label>';
									}
									if (file_exists('areas/'.$radar_id.'/overlays/rivers.png')) {
									    echo '<input id="rivers" type="checkbox" checked="checked"/>';
	                            		echo '<label for="rivers"> Rivers </label>';
									}
									if (file_exists('areas/'.$radar_id.'/overlays/towns.png')) {
									    echo '<input id="towns" type="checkbox" checked="checked"/>';
	                            		echo '<label for="towns"> Towns </label>';
									}
									if (file_exists('areas/'.$radar_id.'/overlays/special.png')) {
									    echo '<input id="special" type="checkbox" checked="checked"/>';
	                            		echo '<label for="special"> Radar </label>';
									}
									if (file_exists('areas/'.$radar_id.'/overlays/shadow.png')) {
									    echo '<input id="shadow" type="checkbox" checked="checked"/>';
	                            		echo '<label for="shadow"> Shadow </label>';
									}
	                        	?>
	                        </div>
	                        <div class="clear">
	                            <!-- clear -->
	                        </div>
	                        <p>
	                            <span id="picTitle"> </span> 
	                        </p>
	                    </div>
                    </div>
                    <div class="justify">
                    	<?php 
                    		include 'inc/flags.php';
                    		include 'lang/'.$lang.'/'.$radar_id.'.php';
                    	?>
                    </div>
                    <div id="help" class="hidden">
                    	<h3>How to use the control buttons</h3>
                    	<p>
                    		The buttons on the left of the <i>help</i> button you just clicked<br/>upon are used to control the animation.
                    	</p>
                    	<p>
                    		There are two states available:
                    	</p>
                    	<h4>Animation is running</h4>
                    	<p>
                            <span class="button_control button_slower" title="Slower animation"> <!-- slower --> </span>
                            <span class="button_control button_stop" title="Pause"> <!-- pause --> </span>
                            <span class="button_control button_faster" title="Faster animation"> <!-- faster --> </span>
                            <div class="clear"><!-- clear --></div>
                    	</p>
                    	<p>
                    		The animation is running if you see the <i>pause</i> button.<br/>In that case, the buttons left and right from the <i>pause</i> button<br/>are used to increase or decrease the animation speed.
                    	</p>
                    	<h4>Animation is paused</h4>
                    	<p>
                            <span class="button_control button_previous" title="Previous picture"> <!-- previous --> </span>
                            <span class="button_control button_play" title="Play"> <!-- play --> </span>
                            <span class="button_control button_next" title="Next picture"> <!-- next --> </span>
                            <div class="clear"><!-- clear --></div>
                    	</p>
                    	<p>
                    		You may stop the animation by pressing the <i>pause</i> button.<br/>In that case, the buttons left and right from the <i>pause</i> button<br/>are used to manually show the next or previous image.
                    	</p>
                    </div>
