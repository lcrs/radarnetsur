<?php 
function checkFolder($pfad){
    $handle = opendir($pfad);
    while ($file = readdir ($handle)) {
		if($file != "." && $file != ".."){
    		echo 1;
			return;
		}
    }
    closedir($handle);
	echo 0;
	return;
}
if(!$folder){
	$folder = htmlspecialchars($_GET["folder"]);
}
if($folder != "" && !strrpos($folder, "..") && substr($folder,0,1) != "/"){
	checkFolder($folder);
}else{
	echo "Fehler! Zu wenig Parameter!";
}
?>